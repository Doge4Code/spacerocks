{
    "id": "24401aef-2d35-4af2-b909-e854c2644d5f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4b32117d-e009-4020-af13-562ed580f86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0147bab6-6574-4da7-be17-d862ea65aef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "777c446a-00be-45df-bf34-f27bc760b8a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d95b4ed6-5b71-48fc-be98-04c20ca8e38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 236,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "36eefd8e-a98e-4d75-b12d-065667c76f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 224,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "69d60c10-18d6-491f-9ae0-f645652cb5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9ff3071b-41d3-42d4-bcca-bb4fb71a804d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 198,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1ce038e5-b097-4183-93d9-6523669dd7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "58e5284f-c487-48fc-ba97-8717b57c5ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 7,
                "x": 183,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2be4f6b8-137a-4f71-8abb-4036d7d2e601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 175,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e304fbe3-f0ab-4fe2-8a95-937c5936379a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "36350046-f239-4ec3-baa6-9fce5465d732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 163,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2b879058-e16d-45e3-907d-7af934f5fc0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 144,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "26ad116f-dae6-4030-bbf6-cb06e77bb8af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 135,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "487f8b68-fba6-40b1-86f7-e687b611092b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bb73ce44-7b87-4bb9-b318-e12a697e2a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "89bb4c86-2c39-4f1a-871f-d893eb2c413f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 105,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "26e4b399-322f-4b3a-8180-f41605c0b435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 94,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d5ab832f-170f-4c24-a3eb-e0d18afdb54a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1eb8e057-2cae-4890-9bdf-164677d6e115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "882f5948-153d-42df-bfa3-87a907c30d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 59,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9d9acc52-78c1-4907-ae3a-6f9716620e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 152,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a64a4c07-f37d-43f0-8b28-f5389e640320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9255d542-ff71-4bc9-bfbb-629cffa73a33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1b815358-1b11-47c9-90f1-73c31bd7f114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a058eb54-8de6-447a-a2df-82b6bffda199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1abc0511-4be1-4d9a-a32e-f42af4f4eec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 4,
                "x": 32,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0073fc9c-04b2-486a-8bd8-088166b15d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 24,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e680a318-aacb-47d5-9026-bdf81c0e32a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "42a0be61-bd94-44e4-9971-a936b14d96dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "459c4715-6d35-454e-a4e3-e7df1ddc0722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 236,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "29dd814c-b38d-44ed-845f-65218107c3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 7,
                "x": 227,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f4fb6cbb-c3f8-4adb-a357-cf0fb84ea28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 214,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2936361a-60de-47f0-bf7b-3eabd2fadc58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "27c5865d-9f01-4698-9626-f1a26ac00803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 190,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b781cccb-0e1e-4702-b126-33774f7df8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "44ad9bca-d07e-49dc-a0a7-49046c4ac70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 166,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "86b26998-c276-40ac-9565-252127ae569a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 155,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4dd4d98c-a4f9-4637-b5d8-1382e1fd7dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dd087bfa-0089-447b-bc1c-636eb02f09bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 133,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cdf16b71-657e-4e75-bf8f-970de4830939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 121,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b2be6618-dca2-4d6f-8acb-f475efe83c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 110,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cbc62701-7ac0-455f-b1c5-e818ee57b047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "66c38260-bb57-46e9-81a5-2618c39a52b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4baa1635-a994-4a16-b26f-60932d3301b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7616b02c-3080-4748-bb18-e5f508464667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "42f78864-2371-4555-935a-99b094b0e1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d701199c-0f37-4b49-9266-b0111501e3c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 34,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fa6a3c78-6014-4cbe-9999-ed6702b7a104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 23,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8af5e687-f194-4bdf-aba9-4fd2626bd052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f4a6ad49-2c55-44e4-a087-e43486e6f2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3281edda-ae81-4954-b4ec-69a65ca0fd6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e1bb3ab3-d989-42b2-9979-ba08fb678a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5b0e39e0-7676-478e-b752-0c1c864cc9ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ac634e30-d53d-4b3c-a13d-aa7e45abe962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e6c7a712-3647-4235-83cf-0fb65af70178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5015d833-ea15-4608-a4db-e1b998598c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2964eb09-e153-4797-b03b-3c64234595bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f272ebf0-90b9-460c-978b-d4e5dda3a47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1b94e66f-9d8b-4d7d-b28d-86717e9a741e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3533b44d-f6eb-4e95-bf5c-df1d26f4c13e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "47551e7f-b19b-444b-a867-88f4390c7e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0e759551-91e0-40c6-953f-ba18c3aac458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "62ae6df0-96be-45b5-b5ad-83c2bd092e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "523ce105-b956-4d36-9e02-51ac06a4b2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "42884935-8673-4639-abc9-de3324f166d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a5d36893-e127-4aff-a178-7fbdcac257a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f9eacbce-dda8-46b5-b61d-6aea3a192fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2faf4960-8a7e-43c9-8ba8-2ad0b84d51d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0ff1a406-02dc-45a3-9221-bd81c2cc06cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1df56b7d-4eb8-415c-a7e3-61730f4177d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ef708e8d-68e3-4de3-a62e-a5ce0ec16fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 15,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4055d83b-a942-4ff1-b9ae-7eeddf503ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 137,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d8586d24-57c6-4297-9348-b1cc3ca39d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 27,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6000320b-def9-472e-88f1-097b69e7545b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fc58958f-8965-4281-957b-0a7d7113b6ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 240,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f9560641-79e9-4c43-a0e9-319c0795bfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "269da4ff-084c-4785-9e32-61d1137d89e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 217,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "354a1332-511d-4e74-982b-18a4de488d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 206,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "67a9318a-1e5f-4dc9-b0ed-5d850438de8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "481f646a-a4b7-4898-8cfb-bad4114a5572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 183,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5bdf7f42-c5da-4880-918b-5cc28d97b936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 171,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "40fc488d-8c34-4bd6-a4d2-b67f7c4f9219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1fbf3ea2-33d2-40df-ad3a-9bfe474009dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ebbdb7c8-81bb-440f-9b65-960996bd177b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 148,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8da3c2f8-1299-44da-a976-e2b8beb4d5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 126,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e0888e52-c1f4-4cf4-b454-e9a0f38b7131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 113,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7dc8b2cb-08fa-417b-9aff-2cba9709f360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7d9e2faa-a7a6-4f3b-8594-4369448799fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "840db19e-0c3e-4475-a873-16d5a5a23d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8d499b2d-0e97-4982-8b27-66832d4e25b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "868688be-5d6c-4630-ab1f-3e39cec81024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 53,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c3b3a7de-61a8-4871-8b34-66400bebb7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 48,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "961945d9-34a8-4aa2-88e6-d70ef9721ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6da0eed7-bd2a-45fc-adce-e082317d3369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 50,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9358f39f-f8b6-4f7a-b3d1-6ed1e44a389a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 63,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
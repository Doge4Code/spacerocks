{
    "id": "d47aae3f-90df-4349-9dc1-b23ac0e9fde5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_broken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb81f957-24fe-41ab-b463-5ad468b11536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d47aae3f-90df-4349-9dc1-b23ac0e9fde5",
            "compositeImage": {
                "id": "dd608238-706d-497c-8fb2-29f0d2a7e64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb81f957-24fe-41ab-b463-5ad468b11536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35e0b515-8f11-4597-b8bc-c2c968cc80df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb81f957-24fe-41ab-b463-5ad468b11536",
                    "LayerId": "d28bdfd5-49c9-42c8-8611-6127df7d0a42"
                }
            ]
        },
        {
            "id": "d47d46c2-6df6-47ee-a5e9-9a65e5e4759d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d47aae3f-90df-4349-9dc1-b23ac0e9fde5",
            "compositeImage": {
                "id": "e1f7ce8e-bd94-48f3-b4a6-802dd5ce8d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47d46c2-6df6-47ee-a5e9-9a65e5e4759d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea7c3ec-3ca0-49ac-8e2d-8400dc00e2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47d46c2-6df6-47ee-a5e9-9a65e5e4759d",
                    "LayerId": "d28bdfd5-49c9-42c8-8611-6127df7d0a42"
                }
            ]
        },
        {
            "id": "ca8db7ba-81b0-4791-bfd2-4a3147b470e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d47aae3f-90df-4349-9dc1-b23ac0e9fde5",
            "compositeImage": {
                "id": "1a62dc28-b760-4b8d-b78a-ac994171a093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8db7ba-81b0-4791-bfd2-4a3147b470e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4b4b7e-62e1-4177-aa5f-00fe36cd5b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8db7ba-81b0-4791-bfd2-4a3147b470e3",
                    "LayerId": "d28bdfd5-49c9-42c8-8611-6127df7d0a42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d28bdfd5-49c9-42c8-8611-6127df7d0a42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d47aae3f-90df-4349-9dc1-b23ac0e9fde5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
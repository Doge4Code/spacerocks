{
    "id": "5f9b422f-ff6c-4502-b598-4e999b584e08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_bar_filling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cea3111-5fb4-4983-b78d-ebd678d3da75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f9b422f-ff6c-4502-b598-4e999b584e08",
            "compositeImage": {
                "id": "0c0980f9-3c82-4d75-acce-59d1927f970d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cea3111-5fb4-4983-b78d-ebd678d3da75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e8c49f-4d64-446c-ab29-e7c58c81f1ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cea3111-5fb4-4983-b78d-ebd678d3da75",
                    "LayerId": "e7ca6dcc-8217-4e16-b32b-16b94f9a6e3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "e7ca6dcc-8217-4e16-b32b-16b94f9a6e3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f9b422f-ff6c-4502-b598-4e999b584e08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
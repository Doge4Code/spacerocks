{
    "id": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_o2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b598509a-9f99-42d0-8927-75084122e44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
            "compositeImage": {
                "id": "38a018cd-47f4-49cf-bf1a-a77e847942dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b598509a-9f99-42d0-8927-75084122e44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f98a1d-5ba2-4c96-a126-2243a4ac2789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b598509a-9f99-42d0-8927-75084122e44b",
                    "LayerId": "ddcf0c29-5da5-4cad-9a78-e4c5e468abfb"
                }
            ]
        },
        {
            "id": "2aa0b28e-6025-4c08-ac1e-03395108f9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
            "compositeImage": {
                "id": "7467ea0b-0ba3-4c1a-afcc-c1663ec555e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa0b28e-6025-4c08-ac1e-03395108f9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a21736f-d860-44cc-a9d2-1b5c8a7fdc83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa0b28e-6025-4c08-ac1e-03395108f9c3",
                    "LayerId": "ddcf0c29-5da5-4cad-9a78-e4c5e468abfb"
                }
            ]
        },
        {
            "id": "b3d31ada-7e12-4c48-a774-143edd408cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
            "compositeImage": {
                "id": "3df9f77a-0c15-4f79-a3f7-dc86e1ec64ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d31ada-7e12-4c48-a774-143edd408cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ea2f13-cd9d-4850-885d-68a4bfbc6ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d31ada-7e12-4c48-a774-143edd408cfc",
                    "LayerId": "ddcf0c29-5da5-4cad-9a78-e4c5e468abfb"
                }
            ]
        },
        {
            "id": "19b17d06-3645-497a-a3eb-a278b5907953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
            "compositeImage": {
                "id": "8566bd98-9d20-44d7-b057-ca196875ca6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b17d06-3645-497a-a3eb-a278b5907953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faed113f-548e-4c57-9c1e-2e1a5074b253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b17d06-3645-497a-a3eb-a278b5907953",
                    "LayerId": "ddcf0c29-5da5-4cad-9a78-e4c5e468abfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "ddcf0c29-5da5-4cad-9a78-e4c5e468abfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}
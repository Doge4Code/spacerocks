{
    "id": "dbe997bd-e48e-47b3-8cbd-c4502408749a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "208019d0-22a1-4911-a8f3-b941ebcb185c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbe997bd-e48e-47b3-8cbd-c4502408749a",
            "compositeImage": {
                "id": "2c5044fb-3c63-4d7c-ac39-06f17cfcd2b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208019d0-22a1-4911-a8f3-b941ebcb185c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34aae96e-2444-49b2-8230-ed8391d8235a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208019d0-22a1-4911-a8f3-b941ebcb185c",
                    "LayerId": "63818995-47f3-4b97-9acd-28af8f899a89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "63818995-47f3-4b97-9acd-28af8f899a89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbe997bd-e48e-47b3-8cbd-c4502408749a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
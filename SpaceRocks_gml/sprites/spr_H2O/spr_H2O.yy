{
    "id": "cce19d20-982d-44b4-8be1-d761cc8534af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_H2O",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21b99a83-0a92-45a5-a4f6-5ff6de06b5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
            "compositeImage": {
                "id": "ca6afc64-34c4-4756-810a-f1cd02a85409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b99a83-0a92-45a5-a4f6-5ff6de06b5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ae5840-2a7f-4b0e-99d3-f793f2bccd7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b99a83-0a92-45a5-a4f6-5ff6de06b5c2",
                    "LayerId": "2bbd001a-b3ff-4b1c-9993-750b326b7f1f"
                }
            ]
        },
        {
            "id": "d0c494bf-d23d-4902-86a4-d3122883ca35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
            "compositeImage": {
                "id": "f5c8b814-f496-425a-affa-752082b43f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c494bf-d23d-4902-86a4-d3122883ca35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cf5712-0a84-4d1d-b533-95b8f44bc647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c494bf-d23d-4902-86a4-d3122883ca35",
                    "LayerId": "2bbd001a-b3ff-4b1c-9993-750b326b7f1f"
                }
            ]
        },
        {
            "id": "aff753bb-f900-404b-affa-621d51d6d548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
            "compositeImage": {
                "id": "fafca485-4ca0-41a3-a173-e46c90327a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff753bb-f900-404b-affa-621d51d6d548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99b26f2c-b446-4b62-8496-520825ba90f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff753bb-f900-404b-affa-621d51d6d548",
                    "LayerId": "2bbd001a-b3ff-4b1c-9993-750b326b7f1f"
                }
            ]
        },
        {
            "id": "55b44d7c-75f2-41ae-9acb-d260868750a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
            "compositeImage": {
                "id": "eca5deba-9643-4e5e-83cc-2e9bb349d84e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b44d7c-75f2-41ae-9acb-d260868750a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ae6d56-b73a-4683-941c-557578f47d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b44d7c-75f2-41ae-9acb-d260868750a8",
                    "LayerId": "2bbd001a-b3ff-4b1c-9993-750b326b7f1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "2bbd001a-b3ff-4b1c-9993-750b326b7f1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}
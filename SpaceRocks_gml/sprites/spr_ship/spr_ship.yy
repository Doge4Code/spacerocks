{
    "id": "77240525-a245-4fe5-a273-8b998ee328c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b09515eb-0d0a-441c-ada8-84cf2365089e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77240525-a245-4fe5-a273-8b998ee328c4",
            "compositeImage": {
                "id": "74ad1f8c-bc18-47d4-b599-50143cf35eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b09515eb-0d0a-441c-ada8-84cf2365089e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec3e12c-7353-4743-8618-f0faf9c53e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b09515eb-0d0a-441c-ada8-84cf2365089e",
                    "LayerId": "50c7b0d4-e4ae-4a94-aa85-d474a8794110"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50c7b0d4-e4ae-4a94-aa85-d474a8794110",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77240525-a245-4fe5-a273-8b998ee328c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
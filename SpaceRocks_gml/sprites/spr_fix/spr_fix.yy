{
    "id": "732f668a-35c1-4c3c-a36c-373730343f2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fix",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd147b2b-9861-4875-b5f7-53c5293c51f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "732f668a-35c1-4c3c-a36c-373730343f2b",
            "compositeImage": {
                "id": "ab891556-f779-49ed-8991-ed989207c34e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd147b2b-9861-4875-b5f7-53c5293c51f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ecdd8c-721a-47a5-bc7c-eb0dc229aa8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd147b2b-9861-4875-b5f7-53c5293c51f7",
                    "LayerId": "26820e1f-9265-450e-bce2-dd0928720e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "26820e1f-9265-450e-bce2-dd0928720e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "732f668a-35c1-4c3c-a36c-373730343f2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 7
}
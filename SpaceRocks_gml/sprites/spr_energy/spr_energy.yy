{
    "id": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "670a8011-af7f-4197-92e5-5db4df7272cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
            "compositeImage": {
                "id": "0568c79b-0ae1-49b1-a842-8b41f65e73bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "670a8011-af7f-4197-92e5-5db4df7272cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2752b7e9-dc36-42b3-8111-4852011cbcc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "670a8011-af7f-4197-92e5-5db4df7272cc",
                    "LayerId": "7b2df7eb-263f-4c71-b352-da0f0a4323b4"
                }
            ]
        },
        {
            "id": "f212f616-13a4-4079-b7dc-b86a701d23a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
            "compositeImage": {
                "id": "830edd34-69a0-42fc-84c0-20d74c54184f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f212f616-13a4-4079-b7dc-b86a701d23a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c171f3f3-f922-4697-b862-7a81e5bca3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f212f616-13a4-4079-b7dc-b86a701d23a6",
                    "LayerId": "7b2df7eb-263f-4c71-b352-da0f0a4323b4"
                }
            ]
        },
        {
            "id": "37c43462-4635-46a0-abf8-86a28d0458b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
            "compositeImage": {
                "id": "fa32f4ed-2a01-4e43-adba-36af747468fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c43462-4635-46a0-abf8-86a28d0458b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d654b9-430b-48c2-9a45-a3ac2138b941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c43462-4635-46a0-abf8-86a28d0458b6",
                    "LayerId": "7b2df7eb-263f-4c71-b352-da0f0a4323b4"
                }
            ]
        },
        {
            "id": "67615c5a-0cee-4e87-af4f-da1e06f71bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
            "compositeImage": {
                "id": "309308cd-8fd9-4d51-a3dc-6b8bac3443e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67615c5a-0cee-4e87-af4f-da1e06f71bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10338faf-76ec-4e32-a72c-da3946ef4290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67615c5a-0cee-4e87-af4f-da1e06f71bad",
                    "LayerId": "7b2df7eb-263f-4c71-b352-da0f0a4323b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "7b2df7eb-263f-4c71-b352-da0f0a4323b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}
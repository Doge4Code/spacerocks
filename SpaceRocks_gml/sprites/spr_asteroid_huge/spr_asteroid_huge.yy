{
    "id": "ab55ff82-88fb-4d1d-9ec1-1dfd5e35d5d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eda2585b-b85e-451e-9019-471ecae2136c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab55ff82-88fb-4d1d-9ec1-1dfd5e35d5d4",
            "compositeImage": {
                "id": "69020822-6f66-4a82-bab3-f469d1c954bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda2585b-b85e-451e-9019-471ecae2136c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d688eee5-8ce9-4f7d-995e-02f73725aac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda2585b-b85e-451e-9019-471ecae2136c",
                    "LayerId": "e557c831-b9a4-4d9d-92d2-125df6fe4ccf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e557c831-b9a4-4d9d-92d2-125df6fe4ccf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab55ff82-88fb-4d1d-9ec1-1dfd5e35d5d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
{
    "id": "0cb44450-e43d-4acd-b027-80ee9fc5b4bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d63ac4b7-c880-45a4-be02-5754f8d3c4b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cb44450-e43d-4acd-b027-80ee9fc5b4bc",
            "compositeImage": {
                "id": "1be892dc-07a1-4376-9612-37b48b49fee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63ac4b7-c880-45a4-be02-5754f8d3c4b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3eb6a22-b1f9-49c0-a63b-b8a75a304685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63ac4b7-c880-45a4-be02-5754f8d3c4b1",
                    "LayerId": "6aecd557-468a-44ff-844c-650559865a41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "6aecd557-468a-44ff-844c-650559865a41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cb44450-e43d-4acd-b027-80ee9fc5b4bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}
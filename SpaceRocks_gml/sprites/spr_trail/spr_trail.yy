{
    "id": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cdec58a-67da-4445-98ca-16498ec43267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
            "compositeImage": {
                "id": "fb827bf7-b4da-435e-82bf-f3e9ad5647b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cdec58a-67da-4445-98ca-16498ec43267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175eb9ad-0fc1-4e44-adcf-bdb5961b3a78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cdec58a-67da-4445-98ca-16498ec43267",
                    "LayerId": "41718b91-0d86-449b-a5f2-0ab95ed12e79"
                }
            ]
        },
        {
            "id": "b1be1cff-dbd0-4dbc-96fe-29b0d77b78aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
            "compositeImage": {
                "id": "096d8059-7911-45a2-8b4e-f4f52849320f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1be1cff-dbd0-4dbc-96fe-29b0d77b78aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b0e412-b536-48aa-86dd-67691006ba34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1be1cff-dbd0-4dbc-96fe-29b0d77b78aa",
                    "LayerId": "41718b91-0d86-449b-a5f2-0ab95ed12e79"
                }
            ]
        },
        {
            "id": "3c4d6d4c-2b0a-4c24-8de7-1fd23377201b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
            "compositeImage": {
                "id": "53a0c48b-30b4-407f-8534-7a7937fad3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c4d6d4c-2b0a-4c24-8de7-1fd23377201b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f58a24-0f83-4e95-9617-ba0721008087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c4d6d4c-2b0a-4c24-8de7-1fd23377201b",
                    "LayerId": "41718b91-0d86-449b-a5f2-0ab95ed12e79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "41718b91-0d86-449b-a5f2-0ab95ed12e79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
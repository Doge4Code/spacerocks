{
    "id": "fb6ba615-6e38-439d-9431-2b9015855af2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_bar_filling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a2fc34a-37c8-476a-b60a-46566e088af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb6ba615-6e38-439d-9431-2b9015855af2",
            "compositeImage": {
                "id": "7de8271f-79b5-4447-8006-b09f0bd2bb38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a2fc34a-37c8-476a-b60a-46566e088af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932b673e-1681-430f-a0b6-7ac829c85c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a2fc34a-37c8-476a-b60a-46566e088af9",
                    "LayerId": "faaa0052-0816-4316-a8ef-2af8d0fa7b97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "faaa0052-0816-4316-a8ef-2af8d0fa7b97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb6ba615-6e38-439d-9431-2b9015855af2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
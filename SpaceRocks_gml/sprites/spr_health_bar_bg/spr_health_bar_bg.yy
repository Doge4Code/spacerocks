{
    "id": "72294f62-19c3-4b6c-a75b-e251fb339021",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_bar_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3e03d26-b69e-4fd4-a547-f93316d02b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72294f62-19c3-4b6c-a75b-e251fb339021",
            "compositeImage": {
                "id": "01e88c74-390f-46e9-8f5f-7ddd3eedd203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e03d26-b69e-4fd4-a547-f93316d02b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40122d1-1a45-4075-831d-7f6a43f5c63d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e03d26-b69e-4fd4-a547-f93316d02b59",
                    "LayerId": "654232c4-232b-4040-b20c-9fbb7ce4ed34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "654232c4-232b-4040-b20c-9fbb7ce4ed34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72294f62-19c3-4b6c-a75b-e251fb339021",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
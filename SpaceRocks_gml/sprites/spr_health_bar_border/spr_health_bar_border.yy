{
    "id": "24e5584d-aa9a-4942-be21-366abfd31e83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_bar_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "123698e9-e1da-4a62-a559-e020ccab0d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24e5584d-aa9a-4942-be21-366abfd31e83",
            "compositeImage": {
                "id": "0c3df84f-2c63-497d-ad5b-7da491229914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123698e9-e1da-4a62-a559-e020ccab0d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130a41f0-192a-4525-8d7b-eb4bc09c748d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123698e9-e1da-4a62-a559-e020ccab0d4b",
                    "LayerId": "3e147b37-cd0d-4fd5-80bc-bdabcc2f3394"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3e147b37-cd0d-4fd5-80bc-bdabcc2f3394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24e5584d-aa9a-4942-be21-366abfd31e83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
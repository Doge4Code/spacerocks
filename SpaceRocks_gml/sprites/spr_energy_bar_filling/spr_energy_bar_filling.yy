{
    "id": "780224ce-40ec-4cdf-b249-f46a7cbcb575",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy_bar_filling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f60218b-5cf6-4be3-833f-3a37b6c6887a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780224ce-40ec-4cdf-b249-f46a7cbcb575",
            "compositeImage": {
                "id": "709274eb-afbe-4256-8c90-d107451ae34b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f60218b-5cf6-4be3-833f-3a37b6c6887a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323fd2c9-fa16-41f6-bac2-be3068454837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f60218b-5cf6-4be3-833f-3a37b6c6887a",
                    "LayerId": "c8f355a2-36dd-4f13-a743-098f3c5fde71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c8f355a2-36dd-4f13-a743-098f3c5fde71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "780224ce-40ec-4cdf-b249-f46a7cbcb575",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
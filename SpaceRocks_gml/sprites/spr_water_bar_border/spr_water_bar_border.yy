{
    "id": "18bfb90e-af25-46a4-a6a6-3bef5530b45d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_bar_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "941cfac0-e379-49eb-8890-97e2117e026f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18bfb90e-af25-46a4-a6a6-3bef5530b45d",
            "compositeImage": {
                "id": "e468e595-91a5-48bf-9edc-8cb7ebaeccbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "941cfac0-e379-49eb-8890-97e2117e026f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd07c79-4e20-4662-9287-3a0aeceb0bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "941cfac0-e379-49eb-8890-97e2117e026f",
                    "LayerId": "1928f83d-4992-4299-94ec-c1f1f7ab6547"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "1928f83d-4992-4299-94ec-c1f1f7ab6547",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18bfb90e-af25-46a4-a6a6-3bef5530b45d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
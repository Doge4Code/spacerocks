{
    "id": "c5b4d9ab-a98a-4a25-84e6-9e62545650bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy_bar_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0e32e76-c189-4ac1-bb1a-638b0fd50e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5b4d9ab-a98a-4a25-84e6-9e62545650bc",
            "compositeImage": {
                "id": "e1d5f9ae-0b2b-4ce6-ab2b-f1525698c0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e32e76-c189-4ac1-bb1a-638b0fd50e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa75f5e-6cc7-486e-8fbd-2985fb0fe9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e32e76-c189-4ac1-bb1a-638b0fd50e59",
                    "LayerId": "8f7f6a6e-d3a2-4317-9161-c0e3c9551f32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "8f7f6a6e-d3a2-4317-9161-c0e3c9551f32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5b4d9ab-a98a-4a25-84e6-9e62545650bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
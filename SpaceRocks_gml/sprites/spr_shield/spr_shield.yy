{
    "id": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc6055ca-2e49-490b-8a76-aa74a15158db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
            "compositeImage": {
                "id": "f5272a67-74cf-49bd-9d8f-afeb05cd4772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6055ca-2e49-490b-8a76-aa74a15158db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83606777-ef06-47c7-b0b3-f11baa4fe2d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6055ca-2e49-490b-8a76-aa74a15158db",
                    "LayerId": "2df26fe9-feda-4ae4-9135-6d29fc0aa058"
                }
            ]
        },
        {
            "id": "742cb69d-7da9-4b0f-b8c1-d1ae2b5f2291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
            "compositeImage": {
                "id": "94821035-392b-464a-9f86-685cf73e7309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742cb69d-7da9-4b0f-b8c1-d1ae2b5f2291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f61628c-7a7a-4c79-b0c3-b1df7e31b428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742cb69d-7da9-4b0f-b8c1-d1ae2b5f2291",
                    "LayerId": "2df26fe9-feda-4ae4-9135-6d29fc0aa058"
                }
            ]
        },
        {
            "id": "5057f74f-6d16-4f4c-bde0-dc8363230951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
            "compositeImage": {
                "id": "924fa820-0f93-4660-9809-d31add64a6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5057f74f-6d16-4f4c-bde0-dc8363230951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5bf066-94be-4eed-b6cb-2d5fb41bc129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5057f74f-6d16-4f4c-bde0-dc8363230951",
                    "LayerId": "2df26fe9-feda-4ae4-9135-6d29fc0aa058"
                }
            ]
        },
        {
            "id": "561af081-a404-41be-b995-194448d25d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
            "compositeImage": {
                "id": "3089f544-4592-414e-b9e1-f3ea987566b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561af081-a404-41be-b995-194448d25d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69dcfa65-505e-42cd-93c0-c2d861759818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561af081-a404-41be-b995-194448d25d58",
                    "LayerId": "2df26fe9-feda-4ae4-9135-6d29fc0aa058"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 128,
    "layers": [
        {
            "id": "2df26fe9-feda-4ae4-9135-6d29fc0aa058",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
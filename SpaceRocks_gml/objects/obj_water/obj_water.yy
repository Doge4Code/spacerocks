{
    "id": "68982bb5-9464-41f3-ace4-53820dcf767b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_water",
    "eventList": [
        {
            "id": "5747be42-3e98-4380-819b-8ad6347c20de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68982bb5-9464-41f3-ace4-53820dcf767b"
        },
        {
            "id": "78cdee53-0801-45a1-ad62-6e9497a9fc5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68982bb5-9464-41f3-ace4-53820dcf767b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cce19d20-982d-44b4-8be1-d761cc8534af",
    "visible": true
}
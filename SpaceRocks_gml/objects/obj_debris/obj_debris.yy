{
    "id": "51b0e11f-3081-46ec-81d6-a43f2da287a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debris",
    "eventList": [
        {
            "id": "da4e6fbc-f866-45b3-bf3b-9f95d32d3841",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51b0e11f-3081-46ec-81d6-a43f2da287a8"
        },
        {
            "id": "a07cdffd-0fda-4f11-9c3d-6e2578416849",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51b0e11f-3081-46ec-81d6-a43f2da287a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0cb44450-e43d-4acd-b027-80ee9fc5b4bc",
    "visible": true
}
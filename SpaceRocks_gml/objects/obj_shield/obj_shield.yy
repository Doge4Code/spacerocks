{
    "id": "e7fe412f-f93b-4e3f-ab23-1593e2d32ba9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shield",
    "eventList": [
        {
            "id": "0b59808e-fa6d-4576-b1a2-446237513257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7fe412f-f93b-4e3f-ab23-1593e2d32ba9"
        },
        {
            "id": "178ae753-fb3d-4ed4-9a41-b4571f98c70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e7fe412f-f93b-4e3f-ab23-1593e2d32ba9"
        },
        {
            "id": "1432575b-9523-48cb-9b9d-667fe5e7334f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "719aaadb-3a37-460f-8935-5418d39d23a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e7fe412f-f93b-4e3f-ab23-1593e2d32ba9"
        },
        {
            "id": "1680a91a-e0ec-4aaf-9750-30ff38ad9a34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7fe412f-f93b-4e3f-ab23-1593e2d32ba9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7ac8c65-9ab7-49c7-b1de-29ab26920da9",
    "visible": true
}
if(keyboard_check(vk_left)){
	image_angle = image_angle + 5;
}
if(keyboard_check(vk_right)){
	image_angle = image_angle - 5;
}

if(keyboard_check(vk_up)){
	motion_add(image_angle, 0.05);
	audio_play_sound(snd_move, 2, false);
}
if(keyboard_check(vk_down)){
	motion_add(image_angle, -0.05);
}	

if(keyboard_check_released(vk_up)){
	audio_stop_sound(snd_move)	
}

if(keyboard_check_pressed(vk_space)){
	if (energy >= 10 && !instance_exists(obj_shield)) {
			instance_create_layer(x = obj_ship.x, y = obj_ship.y, "Instances", obj_shield)
			energy -= 10;
		}
	}


move_wrap(true, true, sprite_width/2);
{
    "id": "2f55b1a7-aff5-41e4-a830-27cc3308df48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "58b5253a-e117-4b66-b47c-0d39eac8effd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "b1f5a309-3767-4051-8a8d-830819a0ec96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "719aaadb-3a37-460f-8935-5418d39d23a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "61619563-a679-45cb-9145-fd3ebca1515b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "68982bb5-9464-41f3-ace4-53820dcf767b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "110f4353-4b68-46ed-9b0d-bfed5ddd6139",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "57478c58-3e74-47eb-a4e8-99074b881766",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "73e59ca4-7475-464f-ada8-89663f9427db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "6e89929d-d4d9-4adc-be9e-a6abf6b1129d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "ce658dbd-ef52-4aa7-8b22-870aba09e026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a07c6523-fd50-468f-aaf3-62f741d58e55",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "6f2408b7-29c7-49b3-a4ad-02e143c3bbd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ca91d37-c118-4aff-ad6e-e3a03822f27a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "c634136e-6ff0-402b-8531-d2a278bbef3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        },
        {
            "id": "aafbb621-9d61-4b1c-acc0-499d5ba4f445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "2f55b1a7-aff5-41e4-a830-27cc3308df48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "64c9fe3b-82f5-4b70-b275-e4f3227d5964",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "100",
            "varName": "oxygen",
            "varType": 1
        },
        {
            "id": "5d00536f-d5a6-45f2-a4c7-a1d21fcc22bb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "100",
            "varName": "water",
            "varType": 1
        },
        {
            "id": "e975b33b-c65e-4f50-a04b-e3daf2bb4570",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "100",
            "varName": "energy",
            "varType": 1
        },
        {
            "id": "1fec3377-a987-4f9d-b4f9-56eaf62b68f8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "ocr",
            "varType": 1
        },
        {
            "id": "79d182c4-3f77-44bf-9104-df377dedbff2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "wcr",
            "varType": 1
        },
        {
            "id": "cf17eb4d-1424-476f-9175-3da56e37b9a4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "ecr",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "77240525-a245-4fe5-a273-8b998ee328c4",
    "visible": true
}
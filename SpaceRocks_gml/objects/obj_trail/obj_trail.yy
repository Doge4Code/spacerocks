{
    "id": "c527e470-5c00-4fb7-9a47-149c8dc13f5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trail",
    "eventList": [
        {
            "id": "fb7e90d8-f0f4-476b-a28a-b81a698fd0c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c527e470-5c00-4fb7-9a47-149c8dc13f5f"
        },
        {
            "id": "58aa5abd-cca9-44af-ae8d-0c94cc3fdb2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c527e470-5c00-4fb7-9a47-149c8dc13f5f"
        },
        {
            "id": "65ea49d5-0763-423a-a8a0-842008b561e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c527e470-5c00-4fb7-9a47-149c8dc13f5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5061ef23-6e8e-43e1-9c3e-1be1a22a3ca4",
    "visible": true
}
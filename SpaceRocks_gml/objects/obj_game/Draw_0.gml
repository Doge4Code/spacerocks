switch(room){
	case rm_game:
	draw_set_halign(fa_center);
		draw_text (250, 15, 
		@"REMAINING TIME: "+string(score)
		);	
			
		draw_set_halign(fa_left);
		
		draw_sprite(spr_health_bar_bg, 1, 15, 15);
		
		if (instance_exists(obj_ship)){
		draw_sprite_ext(spr_health_bar_filling, 1, 15, 15, obj_ship.oxygen/100, 1, 0, c_white, 1)
		}
		
		draw_sprite(spr_health_bar_border, 1, 15, 15);
		
		draw_sprite(spr_health_bar_bg, 1, 15, 40);
		
		if (instance_exists(obj_ship)){
		draw_sprite_ext(spr_water_bar_filling, 1, 15, 40, obj_ship.water/100, 1, 0, c_white, 1)
		}
		
		draw_sprite(spr_water_bar_border, 1, 15, 40);
		
		draw_sprite(spr_health_bar_bg, 1, 15, 65);
		
		if (instance_exists(obj_ship)){
		draw_sprite_ext(spr_energy_bar_filling, 1, 15, 65, obj_ship.energy/100, 1, 0, c_white, 1)
		}
		
		draw_sprite(spr_energy_bar_border, 1, 15, 65);

		break;
		
	case rm_start:
		draw_set_halign(fa_center);
		var c = c_yellow;
		draw_text_transformed_color(
		room_width/2, 100, "SPACE SURVIVAL",
		3 ,3, 0, c,c,c,c, 1
		);
		draw_text(
			room_width/2, 200,
			@"Survive 3 minutes to win!

UP: move
DOWN: slow down
LEFT/RIGHT: change direction
SPACE BAR: activate energy shield

>>PRESS ENTER TO START<<


Press [S] for story
"	
);
		draw_text_transformed(
		room_width/2, 475, "Copyright Codin G Bros",
		.5 ,.5, 0
		);
		draw_set_halign(fa_left);
		break;
		
		case rm_win:
		draw_set_halign(fa_center);
		var c = c_lime;
		draw_text_transformed_color(
		room_width/2, 180, "YOU WIN", 
		3 ,3, 0, c,c,c,c, 1
		);
		draw_text(
			room_width/2, 300,
			">>PRESS ENTER TO RESTART<<"	
		);
		draw_set_halign(fa_left);
		break;
		
		case rm_gameover:
		draw_set_halign(fa_center);
		var c = c_red;
		draw_text_transformed_color(
		room_width/2, 150, "GAME OVER",
		3 ,3, 0, c,c,c,c, 1
		);
		
		draw_text(
		room_width/2, 250,"YOU HAD "+string(score)+" SECONDS LEFT TO WIN"
		);		
		draw_text(
			room_width/2, 300,
			">>PRESS ENTER TO RESTART<<"
		);
		draw_set_halign(fa_left);
		break;
		
		case rm_story:
		draw_set_halign(fa_center);
		draw_text(
			room_width/2, 15,
			@"STORY:
In 2358 humanity has 
completely colonized the 
Milky Way. During their ad-
venture they discovered 22 in-
tellingent species. During a tra-
ding jorney between the humans and 
the mystics the starcruiser was attacked 
by Drac pirates. Most of the crew got wiped 
out by the pirates but some managed to escape 
in escapepods. However they are running out of 
air, water, and energy. 

They must find asteroids 
that carry these vital resources 
to their survival while avoiding other 
asteroids that would damage the ship on impact.

>>PRESS ENTER TO START"
		);

}




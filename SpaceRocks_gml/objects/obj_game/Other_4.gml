if(room == rm_game){
	
	if(audio_is_playing(msc_music)){
		audio_stop_sound(msc_music);	
	}
	
	audio_play_sound(msc_music, 2, true);
	
	repeat(6){
		var xx = choose(
			irandom_range(0, room_width*0.3),
			irandom_range(room_width*0.7, room_width)
		);
	
		var yy = choose(
			irandom_range(0, room_height*0.3),
			irandom_range(room_height*0.7, room_height)
		);
		instance_create_layer(xx, yy, "Instances", obj_asteroid);
		
	}
	
	alarm[0] = 60;
}

if(room == rm_game){

	repeat(1){
		var xx = choose(
			irandom_range(0, room_width*0.3),
			irandom_range(room_width*0.7, room_width)
		);
	
		var yy = choose(
			irandom_range(0, room_height*0.3),
			irandom_range(room_height*0.7, room_height)
		);
		random_resource = choose(1, 2, 3);
		switch(random_resource)
		{
			case 1:
				instance_create_layer(xx, yy, "Instances", obj_oxygen);
				break;
			case 2:
			   instance_create_layer(xx, yy, "Instances", obj_water);
			   instance_create_layer(xx, yy, "Instances", obj_fix);
			   break;
			case 3:
			   instance_create_layer(xx, yy, "Instances", obj_energy);
			   break;
			case 4:
			   instance_create_layer(xx, yy, "Instances", obj_fix);
			   break;
			case 5:
			   instance_create_layer(xx, yy, "Instances", obj_fix);
			   break;
		};
	}
	alarm[2] = 120;
}
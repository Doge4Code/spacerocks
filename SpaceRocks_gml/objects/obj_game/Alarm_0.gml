if(room != rm_game){
	exit;	
}	

if(choose(0, 1) == 0){
	//go down side
	var xx = choose(0, room_width);
	var yy = irandom_range(room_height, 0);
} else {
	//go down bottom	
	var xx = irandom_range(0, room_width);
	var yy = choose( room_height, 0);
}

instance_create_layer(xx, yy, "Instances", obj_asteroid);

alarm[0] = asteroidRate;
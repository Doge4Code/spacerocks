if(room != rm_game){
	exit;	
}	

if(choose(0, 1) == 0){
	//go down side
	var xx = choose(0, room_width);
	var yy = irandom_range(room_height, 0);
} else {
	//go down bottom	
	var xx = irandom_range(0, room_width);
	var yy = choose( room_height, 0);
}




random_resource = choose(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
switch(random_resource)
{
	case 1:
	case 2:
	case 3:
	case 4:
		instance_create_layer(xx, yy, "Instances", obj_water);
		break;
	case 5:
	case 6:
	case 7:
	   instance_create_layer(xx, yy, "Instances", obj_energy);
	   break;
	case 8:
	case 9:
	   instance_create_layer(xx, yy, "Instances", obj_oxygen);
	   break;
	case 10:	   
	   instance_create_layer(xx, yy, "Instances", obj_fix);
	   break;
};

alarm[2] = creationRate;
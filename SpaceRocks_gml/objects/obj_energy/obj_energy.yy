{
    "id": "a07c6523-fd50-468f-aaf3-62f741d58e55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_energy",
    "eventList": [
        {
            "id": "071e2f15-0fed-40fe-9ca0-4eca2b7c16d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a07c6523-fd50-468f-aaf3-62f741d58e55"
        },
        {
            "id": "ef30ac86-7322-430b-a8b1-5a988d84e7d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a07c6523-fd50-468f-aaf3-62f741d58e55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc2b0f2f-0443-402a-acd7-0245abc009c2",
    "visible": true
}
{
    "id": "57478c58-3e74-47eb-a4e8-99074b881766",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_oxygen",
    "eventList": [
        {
            "id": "1081bd3b-e3b1-4aaf-b017-58f3880a8b73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57478c58-3e74-47eb-a4e8-99074b881766"
        },
        {
            "id": "6056c6d6-eb35-49c4-a4c8-f4fa64657466",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57478c58-3e74-47eb-a4e8-99074b881766"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "bb3eed0f-3150-4117-8130-75af4d16ea5b",
    "visible": true
}
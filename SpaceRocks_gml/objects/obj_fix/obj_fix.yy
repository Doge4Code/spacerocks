{
    "id": "5ca91d37-c118-4aff-ad6e-e3a03822f27a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fix",
    "eventList": [
        {
            "id": "5479652f-1057-45d6-8230-1fa2dbbca388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ca91d37-c118-4aff-ad6e-e3a03822f27a"
        },
        {
            "id": "6002ec75-7b87-414e-a110-edd3237988eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ca91d37-c118-4aff-ad6e-e3a03822f27a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "732f668a-35c1-4c3c-a36c-373730343f2b",
    "visible": true
}
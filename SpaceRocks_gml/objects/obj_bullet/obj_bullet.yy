{
    "id": "4b9d6d76-083c-4684-8b21-381ee5b7c543",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "773abb4a-72db-428c-a59c-0c1ef4889eba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b9d6d76-083c-4684-8b21-381ee5b7c543"
        },
        {
            "id": "8e7a9d9f-2911-4723-a408-a9e95be4d5d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "719aaadb-3a37-460f-8935-5418d39d23a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4b9d6d76-083c-4684-8b21-381ee5b7c543"
        },
        {
            "id": "88606511-09af-4951-9671-427493652d81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "4b9d6d76-083c-4684-8b21-381ee5b7c543"
        },
        {
            "id": "20cc578d-68cb-4278-b8df-c3900b2b2612",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "68982bb5-9464-41f3-ace4-53820dcf767b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4b9d6d76-083c-4684-8b21-381ee5b7c543"
        },
        {
            "id": "c94ee23d-e725-4bb4-ae91-3f5bfbf7cecd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "57478c58-3e74-47eb-a4e8-99074b881766",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4b9d6d76-083c-4684-8b21-381ee5b7c543"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
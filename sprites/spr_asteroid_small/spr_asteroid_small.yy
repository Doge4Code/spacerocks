{
    "id": "aca8810f-3e9f-4627-a3f3-55b21afbe5f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db6e3dec-708d-4335-938e-12ba9005aec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca8810f-3e9f-4627-a3f3-55b21afbe5f0",
            "compositeImage": {
                "id": "1ae6d4ce-8dbb-4ade-8326-51889552bfd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6e3dec-708d-4335-938e-12ba9005aec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ea2d6c-f362-4644-b816-61b8ef6ac4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6e3dec-708d-4335-938e-12ba9005aec6",
                    "LayerId": "92405dd8-7144-4598-bb2e-f6d56617e55e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "92405dd8-7144-4598-bb2e-f6d56617e55e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aca8810f-3e9f-4627-a3f3-55b21afbe5f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
{
    "id": "101bcda5-8bcb-4bf1-ab43-0f8191722922",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03a3883d-9654-443a-8cc9-c27a74b95141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "101bcda5-8bcb-4bf1-ab43-0f8191722922",
            "compositeImage": {
                "id": "223c5b93-688a-496c-96fc-c2bebff390e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a3883d-9654-443a-8cc9-c27a74b95141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569d8d5e-7cee-4fff-9557-6c6503023db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a3883d-9654-443a-8cc9-c27a74b95141",
                    "LayerId": "7a9a4f9d-5e3a-4c97-94a3-ae31e16a5c1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a9a4f9d-5e3a-4c97-94a3-ae31e16a5c1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "101bcda5-8bcb-4bf1-ab43-0f8191722922",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
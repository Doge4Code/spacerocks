{
    "id": "b862f709-0b96-406c-81bc-3b10daaa050b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 9,
    "bbox_right": 61,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfaca9ae-dabc-4484-9f8e-625fb0fad2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b862f709-0b96-406c-81bc-3b10daaa050b",
            "compositeImage": {
                "id": "8ce0774c-098d-45cf-92ed-8b6ac13cf7b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfaca9ae-dabc-4484-9f8e-625fb0fad2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea9e1aba-534b-4736-95c5-ccd678ef3f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfaca9ae-dabc-4484-9f8e-625fb0fad2f3",
                    "LayerId": "4d527e04-e32c-42ce-ab6a-b41769d136e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4d527e04-e32c-42ce-ab6a-b41769d136e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b862f709-0b96-406c-81bc-3b10daaa050b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}